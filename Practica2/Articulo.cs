﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2
{
    class Articulo
    {
        public int Clave { get; set; }
        public String Descripcion { get; set; }
        public double Precio { get; set; }
        public int Cantidad { get; set; }


        public Articulo(int clave, string descripcion)
        {
            Clave = clave;
            Descripcion = descripcion;

        }

        internal double CalcularIVA()
        {
            return Precio = Precio * 0.13 * Cantidad;
        }


    }
}
