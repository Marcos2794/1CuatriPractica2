﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2
{
    class CambioDivisas
    {

        public double Colones { get; set; }
        public double TipoCambio { get; set; }

        public CambioDivisas(double tipocambio)
        {
            TipoCambio = tipocambio;
        }

        internal double ConvertirADolares()
        {
            return Colones / TipoCambio;
        }
    }
}
