﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2
{
    class Program
    {
        static void Main(string[] args)
        {
            //------------------ Ejercicio 1 y 2
            Circunferencia moneda = new Circunferencia
            {
                Radio = 1.4

            };

            Circunferencia rueda = new Circunferencia { Radio = 10.2 };

            Console.WriteLine(moneda.Radio);
            Console.WriteLine("El area total es: " + rueda.CalcularArea());
            Console.WriteLine("El area total es: " + rueda.CalcularPerimetro());
            Console.WriteLine("El area total es: " + moneda.CalcularArea());
            Console.WriteLine("El area total es: " + moneda.CalcularPerimetro());


            //------------------ Ejercicio 3 y 4
            Rectangulo calcular = new Rectangulo
            {
                ancho = 4,
                largo = 5,
                largov = 2,
                anchov = 1
            };


            Console.WriteLine("El area total pared es: " + calcular.CalcularAreaTotal() + " metros");
            Console.WriteLine("El area ventana es: " + calcular.CalcularAreaVentana() + " metros");
            Console.WriteLine("El a pintar es: " + calcular.CalcularArea() + " metros");
            Console.WriteLine("El tiempo estimado es: " + calcular.CalcularTiempo() + " horas");


            //------------------ Ejercicio 5

            Fecha fecha = new Fecha();

            Console.WriteLine(fecha.Dias + "/" + fecha.Meses + "/" + fecha.Años);
            


            //-----------------Ejercicio 6

            Articulo art = new Articulo();


            //-----------------Ejercicio 7

            Temperatura temp = new Temperatura();


            Console.ReadKey();
        }
    }
}
