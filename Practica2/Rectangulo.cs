﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica2
{
    class Rectangulo
    {
        public double largo { get; set; }
        public double ancho { get; set; }

        public double largov { get; set; }
        public double anchov { get; set; }

        public double tiempo { get; set; }

        internal double CalcularAreaTotal()
        {

            return (largo * ancho);
        }

        internal double CalcularAreaVentana()
        {

            return (largov * anchov);
        }

        internal double CalcularArea()
        {
            double total =  CalcularAreaTotal()-CalcularAreaVentana();
            return (total);
        }

        internal double CalcularTiempo()
        {
            double tiempo = (CalcularArea() * 10)/60;
            return (tiempo);
        }

    }
}
